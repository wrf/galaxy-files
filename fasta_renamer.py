#!/usr/bin/env python
#
# v1.0 for galaxy pipelines 2015-02-27

# script to rename fasta files

import sys
import argparse
import os

'''
fasta_renamer.py -a "_S1" seqsfile.fa > renamedseqs.fa

fasta_renamer.py -p NAME -n seqsfile.fa > renamedseqs.fa
thus:
>Locus_1_Transcript_1/15
TTGGATATCACCAATAGTTAGGGCGACGAAGCTGTCATTATTACGCGTGTAATATCCGAG
will print as:
>NAME_000001_Locus_1_Transcript_1/15
TTGGATATCACCAATAGTTAGGGCGACGAAGCTGTCATTATTACGCGTGTAATATCCGAG
'''

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', help="fasta format file")
	parser.add_argument('-a','--append', help="string to append to each seq ID", default="")
	parser.add_argument('-p','--prepend', help="string to prepend to each seq ID", default="")
	parser.add_argument('-n','--number', action='store_true', help="prepend a unique number to each ID")
	parser.add_argument('-d','--delimiter', help="delimiter for split, default ' '", default=" ")
	parser.add_argument('-r','--replace', help="replace each character in a string with '_'")
	parser.add_argument('-s','--split', type=int, help="split piece using delimiter -d, ex: 0", default=None)
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	parser.add_argument('-x','--xreplace', action='store_true', help="replace nucleotide sequence X with N")
	args = parser.parse_args(argv)

	if args.number:
		fastacount = 0
		with open(args.input_file, 'r') as fastainf:
			for line in fastainf:
				if line[0]==">":
					fastacount += 1
		# literally counting the number of digits in the length
		reclog = str(len(str(fastacount)))
		# extract version information from sys, and change string accordingly
		# for python 2.7 or greater, use "_{:06}_" instead of "_{0:06}_", as field identifiers are not required
		if sys.version_info[1] >= 7:
			fstring = "{:0"+reclog+"}"
		else:
			fstring = "{0:0"+reclog+"}"
	count = 0

	# this section added just for galaxy
	# check if split is not None and convert from normal index (1) to python index (0)
	if args.split != None:
		pythonsplit = args.split-1
	if pythonsplit < 0:
		pythonsplit = 0

	with open(args.input_file, 'r') as fastainf2:
		for line in fastainf2:
			if line[0]==">":
				count+=1
				# take everything but the first character
				fastaline = line[1:].rstrip()
				# changed to nonetype detection, to allow for splitting index 0
				if not args.split == None:
					# try to split anyway, if it does not work, then ignore and continue
					try:
						fastaline = fastaline.split(args.delimiter)[pythonsplit]
					except IndexError:
						pass
					# because velvet transcripts are in the form of:
					# Locus_2478_Transcript_1/4_Confidence_0.444_Length_2024
					# if the "length_xx" part of the name should be removed, use:
					#seq.id = outdir_name+fstring.format(count)+seq.id.rsplit("_",2)[0]
				if args.replace:
					for n in args.replace:
						fastaline = fastaline.replace(n,"_")
				if args.number:
					outitemlist = [args.prepend, fstring.format(count), fastaline, args.append]
				else:
					outitemlist = [args.prepend, fastaline, args.append]
				modline = ">" + "_".join([x for x in outitemlist if x])
				print >> wayout, modline
			else:
				if args.xreplace:
					wayout.write(line.replace("X","N"))
				else:
					wayout.write(line)

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

