#! /usr/bin/env python
#
# filter_illumina.py v0.1.1 2015-02-25
__version__ = "0.1.1"

import sys
import os
import subprocess

usage="""
filter_illumina.py diagnostics.txt [all other bl-filter-illumina args]
such as:
filter_illumina.py diagnostics.txt -i file1 -i file2 -o output1 -o output2 -u unfiltered -a -b -q 28
"""

def main(argv):
	# get correct filter directory
	biolitedir = os.path.dirname(__file__)
	filter_path = os.path.join(biolitedir, "bl-filter-illumina")

	# parse the arguments from the xml
	statsfile = argv[1]
	filter_args = [filter_path]
	filter_args.extend(argv[2:])

	# create bools for whether to output adapter and base tags
	write_adapters = "-a" in filter_args
	write_bases = "-b" in filter_args

	# debugging lines
	#print >> sys.stderr, biolitedir, filter_path
	#print >> sys.stderr, " ".join(filter_args)

	# stderr could otherwise be a file object, but needs to be piped with Popen to capture for the log
	filt_out = subprocess.Popen(filter_args, stderr=subprocess.PIPE)
	fstdout, fstderr = filt_out.communicate()

	# get numerical extracts from the stderr to report to log
	fstringextract = [l.split(" ")[1] for l in fstderr.split("\n") if l.find("biolite") > -1]

	# read and create a dictionary of the biolite diagnostics output
	diagdict = {}
	for outline in fstringextract:
		ols = outline.split("=")
		diagdict[ols[0]] = ols[1]

	# write formatted output to file
	# this requires diagnostics format from the modified bl-filter-illumina
	# as the original is missing some output fields
	with open(statsfile,'w') as sf:
		paircount = int(diagdict["pairs"])
		print >> sf, "Total Pairs: %d" % paircount
		print >> sf, "Kept Pairs: {} : {:.2f}%".format(diagdict["pairs_kept"], float(diagdict["pairs_kept"])/paircount*100 )
		# divided 2x to get read count from pairs, then multiplied by 100 to get percentage
		print >> sf, "Kept Unpaired Reads: {} : {:.2f}%".format(diagdict["unpaired_kept"], float(diagdict["unpaired_kept"])/paircount/2*100 )
		print >> sf, "Reads rejected for low quality: {} : {:.2f}%".format(diagdict["reject_quality"], float(diagdict["reject_quality"])/paircount/2*100 )
		# must check for write_bases, otherwise this would have made a KeyError
		if write_bases:
			print >> sf, "Reads rejected for base content: {} : {:.2f}%".format(diagdict["reject_content"], float(diagdict["reject_content"])/paircount/2*100 )
			print >> sf, "Bases: too low/ too high:\n%s" % "\n".join(diagdict["suspicious_bases"][1:-2].replace("[","").split("],"))
			print >> sf, "Reads rejected for polyN repeats: {} : {:.2f}%".format(diagdict["reject_polyns"], float(diagdict["reject_polyns"])/paircount/2*100 )
			print >> sf, "Repeats:\n%s" % "\n".join(diagdict["polyns_types"][1:-1].split(","))
		if write_adapters:
			print >> sf, "Reads rejected for adapters: {} : {:.2f}%".format(diagdict["reject_adapters"], float(diagdict["reject_adapters"])/paircount/2*100 )
			print >> sf, "Adapters:\n%s" % "\n".join(diagdict["adapter_types"][1:-1].split(","))
		print >> sf, "Average quality histogram:\n%s" % "\n".join(diagdict["quality_histogram"][1:-1].split(","))

if __name__ == "__main__":
	main(sys.argv)
