#!/usr/bin/env python
#
# v0.1.0 adapted from fpaligner.py v2.2 2016-06-24 by WRF

# currently set up to:
# automatically generate blast db of correct type (nucl vs prot)
# automatically configure and generate result files based on presets
# use tblastn against the target database (from organism in question)
# analyze the results based on the query list

import sys
import argparse
import os
import time
import re
import cStringIO
import subprocess
from collections import defaultdict,deque
from Bio import SeqIO
from Bio.Blast.Applications import NcbitblastnCommandline
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord

def get_basicseq(seqdict, sn):
	# takes a dictionary of Seq objects and returns seq 'sn'
	try:
		basicseq = seqdict[sn]
	# the most common error is the presence of information after a space
	except KeyError:
		basicseq = seqdict[sn.split(" ")[0]]
	return basicseq

def get_matchseq(match, withblastn, query):
	# match is a string, and query is the string of the sequence
	if withblastn:
		matchseq = ''.join(map(lambda keep,val: val if keep else '-', [z=='|' for z in match], query))
	else:
		matchseq = match.replace(' ','-')
	# returns a string
	return matchseq

def frame_to_orfs(protframe):
	# splits a translated protein frame into ORFs
	# note that the '*'s are excluded, thus ORFs will not have stop codons if back translated
	orfs = protframe.split('*')
	return orfs

def get_transseq(nucleotideseq,frame,gencode):
	# takes nucleotide Seq object, and returns translated Seq object
	intronphase = deque([None,-2,-1])
	intronphase.rotate(len(nucleotideseq) % 3)
	if frame < 0:
		transseq = nucleotideseq.reverse_complement()[(abs(frame)-1):intronphase[(abs(frame)-1)]].translate(table=gencode)
	else:
		transseq = nucleotideseq[((frame)-1):intronphase[(abs(frame)-1)]].translate(table=gencode)
	return transseq

def get_align_piece(matchseq):
	# finds longest continuous piece of the alignment
	afrags = sorted(matchseq.replace('+','-').split('-'), key=lambda l: len(l))
	apiece = afrags[-1]
	# returns string of that piece for string.find()
	return apiece

def get_trans_inframe(sequence, frame, match, gencode):
	# function takes Seq object, integer, string, and string of a number
	# gets longest piece of the alignment
	alignPiece = get_align_piece(match)
	# translates seq or reverse complement as indicated by the frame
	transseq = get_transseq(sequence, frame, gencode)
	preorfs = frame_to_orfs(transseq)
	# generates orfs list if preorf contains the alignment, to restrict frame
	orfs = [orf for orf in preorfs if alignPiece in orf]
	# take longest orf which contains the alignment, last term in length-sorted list
	if orfs:
		longestfragment = sorted(orfs, key=len)[-1]
		# returns Bio.Seq.Seq class
		return longestfragment
	else:
		return 0

def get_outline(x,record):
	rt = record.alignments[x].hit_def # record transcript is a unicode string
	ev = record.descriptions[x].e # evalue is a float
	# ag, alignment gaps, meant to replace al, as al can be derived from len(ms) - v1.5
	ag = record.alignments[x].hsps[0].gaps # alignment gaps is an integer
	#al = record.alignments[x].hsps[0].align_length
	ql = record.query_length # query length is an integer
	#fc = float(al)/float(ql)
	rq = record.query # record query is a unicode string
	#ac = record.alignments[x].accession
	# for blastn, generate match string with letters, otherwise use hsps match v1.4
	if record.application == "blastn":
		ms = get_matchseq(record.alignments[x].hsps[0].match, True, record.alignments[x].hsps[0].query)
	else:
		ms = get_matchseq(record.alignments[x].hsps[0].match, False, None)
	# number of alignment of total number of alignments - unused from v1.3
	#na = str(x+1)+"/"+str(len(record.alignments))
	# this is converted into a string, then back - kept as tuple in v1.4
	#qf = str(record.alignments[x].hsps[0].frame)
	qf = record.alignments[x].hsps[0].frame # query frame is a tuple of two integers
	# uses ag instead of al for item [2] - v1.5
	return [rt, float(ev), int(ag), ql, rq, qf[1], ms]

def get_size_ranges(lengthlist):
	min_size = min(lengthlist)
	max_size = max(lengthlist)
	size_upper_var = float(max_size+1)/min_size
	size_low_var = float(min_size)/(sorted(lengthlist)[-2])
	size_floor_var = float(min_size)/(max_size+1)
	return min_size, max_size, size_upper_var, size_low_var, size_floor_var

def get_alphabet(seqstring):
	dnapercent=sum([seqstring.count(x) for x in "ACTG"])/float(len(seqstring))
	if dnapercent >= 0.8:
		#print >> sys.stderr, "# Auto-detected DNA", time.asctime()
		return 'n'
	else:
		#print >> sys.stderr, "# Auto-detected protein", time.asctime()
		return 'p'

def wrap_sequence(sequence, wrap):
	# space out the string with line breaks every n characters, each '\n' counts as one character
	spacedseq = "".join(sequence[i:i+wrap] + "\n" for i in xrange(0,len(sequence), wrap))
	return spacedseq

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-d','--db', help="data base to blast against with reference KOGs")
	parser.add_argument('-D','--directory', help="directory of Core_genes_Parra.fta", default="~/db/")
	parser.add_argument('-k','--kogs', help="name of kog output file - default is autogenerated")
	parser.add_argument('-r','--results', help="name of human readable results - default is autogenerated")
	parser.add_argument('-s','--summary', help="name of results summary file - default is autogenerated")
	parser.add_argument('-p','--processors', metavar='N', help="number of processors, default: 1", default='1')
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff blast search - default: 1e-19", default=1e-19)
	parser.add_argument('-m','--max-seqs', type=int, help="maximum number of records per blast search - default: 50", default=50)
	parser.add_argument('-v','--verbose', action='store_true', help="verbose output")
	parser.add_argument('-b','--blast', help="blast program, default: tblastn", default='tblastn')
	parser.add_argument('-c','--code', help="genetic code", default=1)
	parser.add_argument('-a','--make-db', action='store_true', help="run makeblastdb automatically")
	args = parser.parse_args(argv)

	# establish default blasting parameters for single commandline run
	blastprogram = args.blast

	blast_db = args.db

	evaluethres = args.evalue
	transcode = args.code

	# auto detect thread count
	threadcount = args.processors

	# CEGMA genes are hardcoded, must change for different systems or if files are moved
	# get db directory
	db_path = os.path.expanduser(args.directory)
	blast_file = os.path.join(db_path,"Core_genes_Parra.fta")
	if not os.path.exists(blast_file):
		raise IOError("cannot find Core_genes_Parra.fta in {db_path}".format() )
	# if data cannot be found, check this website to download again
	# http://korflab.ucdavis.edu/Datasets/cegma/
	#
	outkey = "kog"

	#check for matching KOG descriptions file
	descript_dict = defaultdict(str)
	desc_file = os.path.join(db_path, "Core_genes_Parra_desc.txt")
	if os.path.exists(desc_file):
		with open(desc_file,'r') as df:
			for line in df:
				ls = line.rstrip().split(' ',2)
				descript_dict[ls[1]] = " "+ls[2][:70]
	## start main
	recnum = 0
	nomatch = 0

	## gather the sequences
	#print >> sys.stderr, '# Reading sequences:', time.asctime()
	seq_records = list(SeqIO.parse(blast_file, "fasta"))
	seq_dictionary = SeqIO.to_dict(SeqIO.parse(blast_file, "fasta"))
	#print >> sys.stderr, '# Query contains %d sequences:' % (len(seq_records)), time.asctime()
	db_dictionary = SeqIO.to_dict(SeqIO.parse(blast_db, "fasta"))
	#print >> sys.stderr, '# Database contains %d sequences:' % (len(db_dictionary)), time.asctime()

	# generates dictionary entries for each kog to track size range
	# requires that query entries are in form of seq___key
	prot_key_pairs = [rec.id.split("___") for rec in seq_records]
	key_list = list(set([pkp[1] for pkp in prot_key_pairs]))
	# must iterate through all keys to ensure that non-hits are counted
	# otherwise the iterkeys later will skip those with no hits
	key_hits = {}
	for k in key_list:
		key_hits[k] = []
	# as all lengths of the original set are counted, this works
	keylengthdict = defaultdict(list)
	for rec in seq_records:
		keylengthdict[rec.id.split("___")[1]].append(len(rec.seq))

	# check for correct alphabets
	dbfirststring = db_dictionary.itervalues().next().seq
	dbtype = get_alphabet(dbfirststring)
	if dbtype == 'p':
		#print >> sys.stderr, "# Forcing search to blastp"
		blastprogram = 'blastp'

	# generate blastdb if necessary
	if args.make_db:
		if blastprogram == "blastp":
			makedbargs = ["makeblastdb", "-in", blast_db, "-dbtype", "prot"]
		else:
			makedbargs = ["makeblastdb", "-in", blast_db, "-dbtype", "nucl"]
		with open(os.devnull, 'w') as DEVNULL:
			subprocess.call(makedbargs, stdout=DEVNULL)

	## start blast loop
	startclock=time.asctime()
	starttime= time.time()

	# set up blast parameters
	if blastprogram == 'blastp' or blastprogram=='blastn':
		Blast_Command = NcbitblastnCommandline(cmd=blastprogram, query=blast_file, db=blast_db, evalue=evaluethres, outfmt=5, num_threads=threadcount, max_target_seqs = args.max_seqs)
	else:
		Blast_Command = NcbitblastnCommandline(cmd=blastprogram, query=blast_file, db=blast_db, evalue=evaluethres, outfmt=5, num_threads=threadcount, max_target_seqs = args.max_seqs, db_gencode =transcode)

	#print >> sys.stderr, '# Started BLAST on file',blast_file,'at: ' + startclock
	#print >> sys.stderr, 'COMMAND:', Blast_Command
	# creates handle from standard output of blast command
	result_handle = cStringIO.StringIO(Blast_Command()[0])

	#print >> sys.stderr, '# Started BLAST: ', startclock
	#print >> sys.stderr, '# Finished BLAST: ', time.asctime()
	#print >> sys.stderr, "# BLASTed %d records in %.1f minutes" % (len(db_dictionary), (time.time()-starttime)/60)

	twotime=time.time()
	if result_handle:
		for record in NCBIXML.parse(result_handle):
			recnum+=1
			if len(record.alignments):
				# if any record alignments are present
				# all of the results are returned for processing
				for x,alignment in enumerate(record.alignments):
					# returns a list of transcript, evalue, gaps, query length, query name, frame, match
					outlist = get_outline(x, record)
					# hit data is stored in key_hits dictionary
					kog = record.query.split("___")[1]
					key_hits[kog].append(outlist)
			else:
				nomatch+=1
		result_handle.close()
	else:
		raise IOError("ERROR - no XML BLAST results")
	#print >> sys.stderr, '# Finished parsing records: ', time.asctime()
	#print >> sys.stderr, "# Processed %d records in %.1f minutes" % (recnum, (time.time()-twotime)/60)
	#print >> sys.stderr, "# %d records had hits" % ((recnum-nomatch))
	#print >> sys.stderr, "# %d records had no match" % (nomatch)

	# generate files to write outputs

	# human-readable format results file, containing kog and information about hits and alignment to pull unique identifier numbers for the results from kog_results files by:
	# grep \\$ FILES | cut -d "$" -f 2 | cut -d " " -f 1 > output
	dbbasename = os.path.splitext(os.path.basename(blast_db))[0]
	if args.results:
		resultsfilename = args.results
	else:
		resultsfilename = "%s_%s_results.txt" % (dbbasename, outkey)
	resultsoutfile = open(resultsfilename, 'w')	

	if args.kogs:
		seqsfilename = args.kogs
	else:
		fastafiletag = "seqs.faa"
		seqsfilename = "%s_%s_%s" % (dbbasename, outkey, fastafiletag)
	seqsoutfile = open(seqsfilename, 'w')

	## start kog section
	#print >> sys.stderr, '# Processing %d kogs: ' % (len(key_hits)), time.asctime()

	# this section processes key_hits dictionary
	classcounter = {1:0, 2:0, 4:0, 8:0, 16:0, 32:0, 64:0, 128:0, 256:0}
	hitclassdict = {1:"Positive-Match", 2:"Short-alignment-Match", 4:"Possible-correct-ORF", 8:"Short-ORF", 16:"Nonsense-ORF", 32:"Long-ORF", 64:"Incomplete-ORF", 128:"No-ORFs-Detected", 256:"No-Match-Found"}
	hitclass = 0

	koghits = 0 # count of total hits
	allzero = 0 # allzero for where all evalues are zero

	# sort to keep in numerical order
	for kog in sorted(key_hits.keys()):
		print >> resultsoutfile, "#%s%s" % (kog, descript_dict[kog])
		if key_hits[kog]:
			koghits+=1
			realqfs = key_hits[kog]
			rt_list = [x[0] for x in realqfs]
			# generates list of translated ORFs from blast results
			pre_orf_list = []
			# pulls the sequence from the db_dictionary
			for i,rt in enumerate(rt_list):
				rt_sequence = get_basicseq(db_dictionary, rt.strip()).seq
				# formerly if (args.defaults=='r'):
				if blastprogram == 'blastn' or blastprogram == 'blastp':
					pre_orf_list.append((rt_sequence,i))
				else:
					# within a transcript, there may be cis chimeras, thus the longest piece of the alignment is used to determine the correct ORF in that reading frame and declare that as the protein for the following steps
					longestfragment = get_trans_inframe(rt_sequence, realqfs[i][5],realqfs[i][6],args.code)
					if longestfragment:
						pre_orf_list.append((longestfragment,i))

			# training set size ranges are generated here
			kog_min_size, kog_max_size, size_upper_var, size_low_var, size_floor_var = get_size_ranges(keylengthdict[kog])

			# counts for cases where e value is zero for all hits
			ev_list = [x[1] for x in realqfs]
			if not any(ev_list):
				allzero+=1
			ev_orf_list = []
			# if any preorfs exist, then search for M in those preorfs
			if pre_orf_list:
				# as above for rt_sequence, changed to check for blastn, as nucleotide sequences will have no 'M'
				if blastprogram == 'blastn':
					rt_orf_list = pre_orf_list
				else:
					rt_orf_list = [[orf[0][orf[0].find('M'):],orf[1]] for orf in pre_orf_list if orf[0].count('M')]
				if rt_orf_list:
					ev_orf_list = [realqfs[x[1]][1] for x in rt_orf_list]
					best_ev_index = ev_orf_list.index(min(ev_orf_list))
					final_rt = rt_list[rt_orf_list[best_ev_index][1]]+"___"+kog
					best_sequence = SeqRecord(rt_orf_list[best_ev_index][0], id=final_rt, description=final_rt)
					best_query = realqfs[rt_orf_list[best_ev_index][1]][4]
					best_ev_match = realqfs[rt_orf_list[best_ev_index][1]][6]
					best_ev_gs = realqfs[rt_orf_list[best_ev_index][1]][2]
					best_ev_al = len(realqfs[rt_orf_list[best_ev_index][1]][6])
					best_ev_ql = realqfs[rt_orf_list[best_ev_index][1]][3]
					best_ev_frame = realqfs[rt_orf_list[best_ev_index][1]][5]
					# if using blastn, reverse complement if frame is negative
					if blastprogram == 'blastn' and best_ev_frame < 0:
						best_sequence.seq = best_sequence.seq.reverse_complement()
					# begin scoring of the best hit

					# redefine nonsense as any protein where the length of the protein is less than alignment+gaps
					if best_ev_al-best_ev_gs > len(best_sequence):
						hitclass = 16
					# if size is positive but alignment is too short
					elif (size_upper_var*kog_max_size > len(best_sequence) >= size_low_var*kog_min_size) and ((len(best_sequence) > 1.4*best_ev_al) or (len(best_sequence) < 0.9*best_ev_al)):
						# for example, to calculate low confidence matches
						#if (abs(1-(len(best_sequence)/float(best_ev_al))) <= 0.1) and best_ev_al < 0.9*best_ev_ql:
						# so if P:244 A:232 Q:752, then |1-(244/232)| < 1-(708/753) and 232 < (708/753*708)
						# 1-0.9508 < 1-0.9414 and 232 < 666.57
						# 0.04918 < 0.05851 and True, thus True
						# but if P:168 A:130 Q:190, then 1-(168/130) < 1-(190/198) and 130 < 190/198*190
						# |1-1.2923| < 1-0.9595 and 130 < 182.32
						# 0.2923 > 0.0404 and True, thus False
						hitclass = 2
					# if size is within the expected range
					elif size_upper_var*kog_max_size > len(best_sequence) >= size_low_var*kog_min_size:
						hitclass = 1
					# if size is slightly shorter
					elif size_low_var*kog_min_size > len(best_sequence) >= size_floor_var*kog_min_size:
						hitclass = 4
					# if size is much longer, possibly due to mistakenly taking the first 'M'
					elif len(best_sequence) >= size_upper_var*kog_max_size:
						hitclass = 32
					# if much shorter, or all other proteins
					else:
					# changed to else from elif len(best_sequence) < (size_floor_var*kog_min_size): v1.4
						hitclass = 8
				else:
					ev_orf_list = [realqfs[x[1]][1] for x in pre_orf_list]
					best_ev_index = ev_orf_list.index(min(ev_orf_list))
					final_rt = rt_list[pre_orf_list[best_ev_index][1]]+"___"+kog
					best_sequence = SeqRecord(pre_orf_list[best_ev_index][0], id=final_rt, description=final_rt)
					best_query = realqfs[pre_orf_list[best_ev_index][1]][4]
					best_ev_match = realqfs[pre_orf_list[best_ev_index][1]][6]
					best_ev_al = len(realqfs[pre_orf_list[best_ev_index][1]][6])
					best_ev_ql = realqfs[pre_orf_list[best_ev_index][1]][3]
					best_ev_frame = realqfs[pre_orf_list[best_ev_index][1]][5]
					hitclass = 64
				# line shows protein length (P), alignment length (A), query length (Q), e value, and frame
				print >> resultsoutfile, "$%d %s" % (hitclass, hitclassdict[hitclass])
				print >> resultsoutfile, "P:%d A:%d Q:%d E:%.2e F:%d" % (len(best_sequence), best_ev_al, best_ev_ql, ev_orf_list[best_ev_index], best_ev_frame)
				resultsoutfile.write("%s" % best_sequence.format("fasta"))
				print >> resultsoutfile, ">Alignment"
				resultsoutfile.write(wrap_sequence(best_ev_match, 60) )
				resultsoutfile.write("%s" % seq_dictionary[best_query].format("fasta"))
				seqsoutfile.write("%s" % best_sequence.format("fasta"))
			else:
				hitclass = 128
				print >> resultsoutfile, "$%d %s" % (hitclass, hitclassdict[hitclass])
				print >> resultsoutfile, "P:0 A:0 Q:%d E:0 F:0" % (kog_min_size)
		# if no hits were found for a kog
		else:
			hitclass = 256
			print >> resultsoutfile, "$%d %s" % (hitclass, hitclassdict[hitclass])
			print >> resultsoutfile, "P:0 A:0 Q:%d E:0 F:0" % min(keylengthdict[kog])
		# counting can happen at the end as nothing else depends on it
		classcounter[hitclass]+=1

	# finally, output results to stderr
	if args.summary:
		summaryfile = args.summary
	else:
		summaryfile = "%s_%s_summary.tab" % (dbbasename, outkey)
	with open(summaryfile,'w') as outmode:
		#outmode = sys.stderr
		print >> outmode, "KOGS with hits (out of 248)\t%d" % (koghits)
		print >> outmode, "High-confidence full-length matches ($1)\t%d" % (classcounter[1])
		print >> outmode, "Probable full-length matches ($2)\t%d" % (classcounter[2])
		print >> outmode, "KOGs slightly shorter than the query ($4)\t%d" % (classcounter[4])
		print >> outmode, "KOGs much shorter than the query ($8)\t%d" % (classcounter[8])
		print >> outmode, "Probable nonsense proteins/ misassemblies ($16)\t%d" % (classcounter[16])
		print >> outmode, "KOGs much longer than the query ($32)\t%d" % (classcounter[32])
		print >> outmode, "No predicted orfs within expected size ($64)\t%d" % (classcounter[64])
		print >> outmode, "No Met or stop codons ($128)\t%d" % (classcounter[128])
		print >> outmode, "No hits ($256)\t%d" % (classcounter[256])

	resultsoutfile.close()
	seqsoutfile.close()

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
