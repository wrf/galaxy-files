#! /usr/bin/env python
# v1.1
# filter RSEM results by highest IsoPct for each component
# remove components with IsoPct of 0.00
# v1.2 2015-01-07
# rename fasta sequences to include IsoPct and numbers of seqs per component

import sys
from collections import defaultdict
from Bio import SeqIO

'''
filter RSEM results by highest IsoPct for each component
remove components with IsoPct of 0.00

take_top_rsem_splice.py RSEM.isoforms.results Trinity.fasta output_best_splice.fasta
'''

# where RSEM.isoforms.results as rsf should be argv[1]
# where Trinity.fasta should be argv[2]
# where the output file name as ff should be argv[3]

with open(sys.argv[1],'r') as rsf:
	rsemlines = rsf.readlines()
# remove first header line, readlines is used only to allow pop instead of 'if not line==0'
rsemlines.pop(0)

# stores the best sequence for that component
od = {}
# tracks the maximum score for that component
cd = defaultdict(float)
# counts number of sequences per component
sd = defaultdict(int)

for rl in rsemlines:
	# remove \n and split tabs
	rlsplit = rl.rstrip().split()
	isopct = float(rlsplit[7])

	# add to count for that component
	sd[rlsplit[1]] += 1

	# discard cases where the sole transcript in a component has isopct of 0.00
	# also implies that if isopcts are equal, the first will arbitrarily be taken
	if isopct > cd[rlsplit[1]] and isopct > 0.00:
		cd[rlsplit[1]]=isopct
		od[rlsplit[1]]=rlsplit[0]

seqdict = SeqIO.to_dict(SeqIO.parse(sys.argv[2],'fasta'))
bestsplicefile = sys.argv[3]
hc = 0
with open(bestsplicefile,'w') as ff:
	for k in od:
		# remove len and path from fasta sequence name for trinity
		# and then append the isopct and number of sequences in that component
		# so final should appear like:
		# comp1234_c0_seq1_76.54_3
		newid = "%s_%.2f_%d" % (seqdict[od[k]].id.split(' ')[0], cd[k], sd[k])
		# again, have to set description to empty string
		seqdict[od[k]].id, seqdict[od[k]].description = newid, ""
		try:
			ff.write("%s" % seqdict[od[k]].format("fasta"))
		except KeyError:
			ff.write("%s" % seqdict[od[k].split(" ")[0]].format("fasta"))
