#! /usr/bin/env python
# v1.0
# fastasizecutoff.py
# filter fasta sequences by length
# v1.1 added count of sequences longer than a given length

import sys
from numpy import arange, histogram
from Bio import SeqIO

usage="""
fastasizehistogram.py sequences.fasta histogram.txt 50 0 0
"""

fastafile = sys.argv[1]
outputfile = sys.argv[2]

seqsizes = []
# iterate through each seq record
#print >> sys.stderr, "Reading from %s" % fastafile
for seqrec in SeqIO.parse(fastafile,'fasta'):
	seqlen = len(seqrec.seq)
	seqsizes.append(seqlen)

seqcount = len(seqsizes)

# bin_size for the histogram
bin_size = int(sys.argv[3])
# minval is the lower bound for histogram binning, read in as a string and converted to int
minval = int(sys.argv[4])
# naturally maxval is maximum, kept as string as it is likely to be changed
maxval = sys.argv[5]

if maxval == "0":
	maxval = max(seqsizes)
else:
	maxval = int(maxval)

bins = arange(minval, maxval+bin_size, bin_size)
# values above the max will be changed to the max and put into the last bin
for i,x in enumerate(seqsizes):
	if x >= maxval:
		seqsizes[i] = maxval
# histogram function generates a tuple of arrays, which must be zipped to iterate
sizehist = histogram(seqsizes,bins,(minval, maxval+bin_size))

with open(outputfile,'w') as ht:
	print >> ht, "Length\tCount\tSequences longer than Length"
	for x,y in zip(sizehist[0],sizehist[1]):
		print >> ht, "%d\t%d\t%d" % (y,x,seqcount)
		seqcount -= x
#print >> sys.stderr, "Writing histogram to %s" % outputfile
