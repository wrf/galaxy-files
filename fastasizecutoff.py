#! /usr/bin/env python
# v1.0
# fastasizecutoff.py
# filter fasta sequences by length

import sys
from Bio import SeqIO

usage="""
fastasizecutoff.py sequences.fasta outputfile.fasta 1000
or add an upper limit
fastasizecutoff.py sequences.fasta outputfile.fasta 1000 1000000
"""

fastafile = sys.argv[1]
outputfile = sys.argv[2]
# above is the lower bound cutoff for sequence length, read in as a string and converted to int
above = int(sys.argv[3])

if len(sys.argv) > 4:
	# if below is specified as 3rd command line argument, use it
	# must convert to int like for "above"
	below = int(sys.argv[4])
else:
	# otherwise below defaults to 1 billion
	below = 1000000000

# open the output file
#print >> sys.stderr, "Reading from %s" % fastafile
#print >> sys.stderr, "Writing filtered results to %s" % outputfile

with open(outputfile,'w') as ff:
	# iterate through each seq record
	for seqrec in SeqIO.parse(fastafile,'fasta'):
		seqlen = len(seqrec.seq)
		# retain sequence if above "above" and below "below"
		if below > seqlen >= above:
			ff.write("%s" % seqrec.format("fasta"))
